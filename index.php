<?php
  error_reporting(E_ALL);
  require("./Models/News.php");
  $pubsService = new PubsService();
  $lastNews = $pubsService->getAll(3, null, false);
  $lastPubs = $pubsService->getAll(3, null, true);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Портал корпоративной прозрачности</title>
    <link tyle="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/">Главная<span class="sr-only">(current)</span></a></li>
            <li><a href="#">О нас</a></li>
            <li><a href="/ratings.php">Рейтинги</a></li>
            <li><a href="/pubs.php">Публикации</a></li>
            <li><a href="/news.php">Новости</a></li>
            <li><a href="#">База данных</a></li>
          </ul>
          <ul class="nav navbar-nav pull-right">
            <li><a href="/admin.php">Панель управления</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-sm-7">
          <div style="border-bottom: 4px solid black;margin-bottom:10px;">
            <h2>Текущий рейтинг</h2>
          </div>
          <table class="table table-hover" style="font-size:0.8em;">
            <tr>
              <th class="text-center">Место</th>
              <th>Название</th>
              <th></th>
              <th class="text-center">Индекс</th>
              <th class="text-center">Балл</th>
            </tr>
            <tr>
              <td class="text-center">1</td>
              <td>Акционерное общество НИЖЕГОРОДСКАЯ ИНЖИНИРИНГОВАЯ КОМПАНИЯ «АТОМЭНЕРГОПРОЕКТ»</td>
              <td> </td>
              <td class="text-center">A</td>
              <td class="text-center"><span style="color:green;" class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>92,25<td>
            </tr>
            <tr>
              <td class="text-center">2</td>
              <td>Акционерное общество «Российский концерн по производству электрической и тепловой энергии на атомных станциях»</td>
              <td> </td>
              <td class="text-center">A</td>
              <td class="text-center"><span style="color:green;" class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>89,75<td>
            </tr>
            <tr>
              <td class="text-center">3-4</td>
              <td>Государственная корпорация по атомной энергии «Росатом»</td>
              <td>
              <td class="text-center">A</td>
              <td class="text-center"><span style="color:red;" class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>88,13<td>
            </tr>
            <tr>
              <td class="text-center">3-4</td>
              <td>Акционерное общество «ТВЭЛ»</td>
              <td><span style="color:yellow;" class="glyphicon glyphicon-alert" aria-hidden="true"></span></td>
              <td class="text-center">A</td>
              <td class="text-center"><span style="color:green;" class="glyphicon glyphicon-triangle-top" aria-hidden="true">88,13<td>
            </tr>
            <tr>
              <td class="text-center">5</td>
              <td>Акционерное общество «Атомное и энергетическое машиностроение»</td>
              <td></td>
              <td class="text-center">B</td>
              <td class="text-center"><span style="color:red;" class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>70,5<td>
            </tr>
            <tr>
              <td class="text-center">6</td>
              <td>Публичное акционерное общество «Федеральная гидрогенерирующая компания – РусГидро» </td>
              <td></td>
              <td class="text-center">B</td>
              <td class="text-center"><span style="color:green;" class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>70<td>
            </tr>
            <tr>
              <td class="text-center">7</td>
              <td>Публичное акционерное общество «Горно-металлургическая компания «Норильский никель»</td>
              <td></td>
              <td class="text-center">B</td>
              <td class="text-center"><span style="color:green;" class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>63,88<td>
            </tr>
            <tr>
              <td class="text-center">8</td>
              <td>Акционерное общество «Сибирская Угольная Энергетическая Компания»</td>
              <td></td>
              <td class="text-center">B</td>
              <td class="text-center"><span style="color:green;" class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>55,75<td>
            </tr>
            <tr>
              <td class="text-center">9</td>
              <td>Публичное Акционерное Общество «Нижнекамскнефтехим»</td>
              <td></td>
              <td class="text-center">B</td>
              <td class="text-center"><span style="color:green;" class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>51,5<td>
            </tr>
            <tr>
              <td class="text-center">10</td>
              <td></td>
              <td></td>
              <td class="text-center">B</td>
              <td class="text-center"><span style="color:green;" class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>70<td>
            </tr>
            
          </table>

        </div>
        <div class="col-sm-5">
          <div style="border-bottom: 4px solid black;margin-bottom:10px;">
            <h2>Новости</h2>
          </div>
          <?php if ($lastNews) {?>
            <ul class="media-list">
              <?php foreach ($lastNews as $news) { ?>
                <li class="media">
                  <?php echo $news->render(NEWS_MIN_MODE); ?>
                </li>
              <?php } ?>
            </ul>
            <div class="text-center">
              <a href="/news.php" class="btn btn-lg btn-danger">Больше новостей</a>
            </div>
          <?php } else { ?>
            <p>Новостей пока нет!</p>
          <?php } ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div style="border-bottom: 4px solid black;margin-bottom:10px;">
            <h2>Публикации</h2>
          </div>
          <div class="row">
            <?php if(isset($lastPubs)) :
              foreach ($lastPubs as $pub) :
                echo $pub->render(PUB_MIN_MODE);
              endforeach;
            else : ?>
              <p>Публикаций пока нет</p>
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
  </body>
</html>
