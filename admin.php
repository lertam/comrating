<?php
  require_once('./Models/News.php');
  $pubsService = new PubsService();
  global $error;
  global $success;
  $error = '';
  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if(isset($_POST['title']) && isset($_POST['text'])) {
      $newsData = [
        'title' => htmlspecialchars($_POST['title']),
        'text' => htmlspecialchars($_POST['text']),
        'image' => null,
        'is_publication' => $_POST['is_publication']
      ];
      if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
        $uploaddir = './uploads/';
        $uploadfile = $uploaddir.hash('sha256', $_FILES['image']['tmp_name']).'.jpg';
        if (!copy($_FILES['image']['tmp_name'], $uploadfile)) {
          $error = "Не удалось загрузить изображение";
        }
        else {
          $newsData['image'] = $uploadfile;
        }
      }
      if ($error == '') {
        $res = $pubsService->save($newsData);

        if (!$res) {
          $error= 'Невозможно сохранить в базу.';
          if($newsData['image'] != null) {
            //delete image     
          }
        } 
        else {
          $success = "Запись успешно добавлена!";
        }
      }
    }
  }
  $allPubs = $pubsService->getAll(10, null, null);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Портал корпоративной прозрачности</title>
    <link tyle="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/">Сайт<span class="sr-only">(current)</span></a></li>
            <!-- <li><a href="#">Публикации</a></li>
            <li><a href="/ratings.php">Рейтинги</a></li> -->
          </ul>

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
      <?php if ($error) : ?>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Ошибка!</strong> <?php echo $error; ?>
      </div>
      <?php endif; ?>
      <?php if ($success) : ?>
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $success; ?>
      </div>
      <?php endif; ?>


      <h2>Публикации</h2>
      <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#addNewsForm">
        Добавить
      </button>

      <div id="addNewsForm" class="modal fade">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h1 class="modal-title">Добавить</h1>
            </div>
            <div class="modal-body">
              <form role="form" method="POST" action="" enctype="multipart/form-data">
                <div class="form-group">
                  <div class="radio-inline">
                    <label>
                      <input type="radio" name="is_publication" value="0" checked>
                        Новость
                    </label>
                  </div>
                  <div class="radio-inline">
                    <label>
                      <input type="radio" name="is_publication" value="1">
                      Публикация
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">Название</label>
                  <div>
                    <input required type="text" class="form-control input-lg" name="title" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">Изображение</label>
                  <div>
                    <input type="file" accept="image/*" name="image">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">Текст</label>
                  <div>
                    <textarea required name="text" class="form-control" rows="10"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div>
                    <button type="submit" class="btn btn-success">Добавить</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <?php if (count($allPubs) == 0) : ?>
        <div class="text-center">Новостей нет</div>
      <?php else : ?>
        <table class="table table-hover">
          <tr>
            <th>#</th>
            <th>Название</th>
            <!-- <th>Текст</th>
            <th>Изображение</th> -->
            <th>Создано</th>
            <th>Тип</th>
          </tr>
          <?php foreach ($allPubs as $news) {
            echo $news->render(ADMIN_MODE);
          } ?>
        </table>
      <?php endif; ?>
    </div>

    <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
  </body>
</html>
