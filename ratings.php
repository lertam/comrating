<?php
  error_reporting(E_ALL);
  include_once("./Models/News.php");

  $rating=((isset($_GET['type']) && $_GET['type'] == 'subrating') || isset($_GET['subtype'])) ? 'subrating' : 'overall';
  if($rating == 'subrating') $subrating = (isset($_GET['subtype']) && $_GET['subtype'] == '2') ? '2' : '1';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Портал корпоративной прозрачности</title>
    <link tyle="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="/">Главная</a></li>
            <li><a href="#">О нас</a></li>
            <li class="active"><a href="/ratings.php">Рейтинги<span class="sr-only">(current)</span></a></li>
            <li><a href="/pubs.php">Публикации</a></li>
            <li><a href="/news.php">Новости</a></li>
            <li><a href="#">База данных</a></li>
          </ul>
          <ul class="nav navbar-nav pull-right">
            <li><a href="/admin.php">Панель управления</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1" style="padding-top:15px; padding-bottom:15px;">
            <div class="row">
                <div class="col-sm-12 text-center" style="border-left:5px solid black; border-right: 5px solid black;padding-top:10px;padding-bottom:10px;margin-bottom:20px;">
                    <a <?php echo ($rating == 'overall') ? 'href="?type=subrating">итоговый рейтинг' : 'href="?type=overall">субрейтинг'; ?></a> за <a href="#">2015 год</a>
                    <?php if($rating == 'subrating') : ?>
                        <div class="text-center col-xs-12">
                            <?php if($subrating == '1') : ?>
                                <a href="?subtype=2">Субрейтинг частных компаний</a>
                            <?php else : ?>
                                <a href="?subtype=1">Субрейтинг государственных компаний</a>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <?php if ($rating == 'overall') : ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <!-- <img src="..." alt="..."> -->
                            <div class="caption" style="height:200px">
                                <h3>1</h3>
                                <p>Акционерное общество НИЖЕГОРОДСКАЯ ИНЖИНИРИНГОВАЯ КОМПАНИЯ «АТОМЭНЕРГОПРОЕКТ»</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <!-- <img src="..." alt="..."> -->
                            <div class="caption" style="height:200px">
                                <h3>2</h3>
                                <p>Акционерное общество «Российский концерн по производству электрической и тепловой энергии на атомных станциях»</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <!-- <img src="..." alt="..."> -->
                            <div class="caption" style="height:200px">
                                <h3>3-4</h3>
                                <p>Государственная корпорация по атомной энергии «Росатом»</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <!-- <img src="..." alt="..."> -->
                            <div class="caption" style="height:200px">
                                <h3>3-4</h3>
                                <p>Акционерное общество «ТВЭЛ»</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <!-- <img src="..." alt="..."> -->
                            <div class="caption" style="height:200px">
                                <h3>5</h3>
                                <p>Акционерное общество «Атомное и энергетическое машиностроение»</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <!-- <img src="..." alt="..."> -->
                            <div class="caption" style="height:200px">
                                <h3>6</h3>
                                <p>Публичное акционерное общество «Федеральная гидрогенерирующая компания – РусГидро»</p>
                            </div>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <div class="caption" style='height:200px'>
                                <h3>1</h3>
                                <p>Публичное акционерное общество «Горно-металлургическая компания «Норильский никель»</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <div class="caption" style='height:200px'>
                                <h3>2</h3>
                                <p>Акционерное общество «Сибирская Угольная Энергетическая Компания»</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <div class="caption" style='height:200px'>
                                <h3>3</h3>
                                <p>Публичное Акционерное Общество «Нижнекамскнефтехим»</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <div class="caption" style='height:200px'>
                                <h3>4</h3>
                                <p>Публичное акционерное общество «Акционерная финансовая корпорация «Система»</p>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
                
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-2 text-center">
                    <a href="#">Premium</a>
                </div>
                <div class="col-xs-12 col-sm-2 text-center">
                    <a href="#">I уровень</a>
                </div>
                <div class="col-xs-12 col-sm-2 text-center">
                    <a href="#">II уровень</a>
                </div>
                <div class="col-xs-12 col-sm-2 text-center">
                    <a href="#">III уровень</a>
                </div>
                <div class="col-xs-12 col-sm-2 text-center">
                    <a href="#">IV уровень</a>
                </div>
                <div class="col-xs-12 col-sm-2 text-center">
                    <a href="#">V уровень</a>
                </div>
            </div>
        </div>
      </div>
    </div>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
  </body>
</html>
