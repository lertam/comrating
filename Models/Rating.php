<?php

require_once(__DIR__.'/../config.php');

class Position {
    /**
     * Индекс раскрытия информации
     */
    private $indexes = [
        'A','B','C', //....
    ];
    /**
     * Полное наименование компании
     */
    private $name;

    /**
     * Индекс раскрытия информации
     */
    private $index;
    
    /**
     * Позиция
     */
    private $position;

    /**
     * Балл
     */
    private $score;

    public function __construct (Array $data) {
        try {
            $this->name = $data['name'];
            $this->setIndex($data['index']);
            $this->position = $data['position'];
            $this->score = $data['score'];
        }
        catch (Exception $e) {
            var_dump($e);
        }
    }
     
    public function getName (){
        return $this->name;
    }

    public function getIndex () {
        return $this->index;
    }

    public function setIndex ($index) {
        if (in_array($index, $this->indexes)) {
            $this->index = $index;
        }
        else {
            throw new Exception('Unknown index');
        }
    }

    public function getPosition () {
        return $this->position;
    }

    public function getScore () {
        return $this->score;
    }
}

class Rating {
    private $types = ['overall rating','subrating','subranking'];

    private $type; // Возможно следует использовать объект
    private $date;
    /**
     * Premium уровень прозрачности
     * 
     * @var array Position
     */
    public $premium = array();
    /**
     * I уровень прозрачности
     * 
     * @var array Position
     */
    public $level1 = array();
    /**
     * II уровень прозрачности
     * 
     * @var array Position
     */
    public $level2 = array();
    /**
     * III уровень прозрачности
     * 
     * @var array Position
     */
    public $level3 = array();
    /**
     * IV уровень прозрачности
     * 
     * @var array Position
     */
    public $level4 = array();
    /**
     * V уровень прозрачности
     * 
     * @var array Position
     */
    public $level5 = array();

    public function __contruct (Array $data) {
        try {
        }
        catch (Exception $e) {
            var_dump($e);
        }
    }

    public function getLevel ($level = -1) {
        switch($level) {
            case 0:
                return $this->premium;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return $this->{'level'.$level};
            default:
                return [
                    'premium' => $this->premium,
                    'I' => $this->level1,
                    'II' => $this->level2,
                    'III' => $this->level3,
                    'IV' => $this->level4,
                    'V' => $this->level5
                ];
        }
    }

}

$pos11 = new Position(['name'=>'name1', 'index'=>'A', 'score'=>62.2, 'position' => 1]);
$pos21 = new Position(['name'=>'name2', 'index'=>'A', 'score'=>62.2, 'position' => 23]);


