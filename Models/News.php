<?php

require_once(__DIR__.'/../config.php');

class News {
  private $id = null;
  private $title = null;
  private $text = null;
  private $created = null;
  private $imgUrl = null;
  private $is_publication = false;

  public function __construct (Array $arr) {
    $this->setTitle($arr['title']);
    $this->setText($arr['text']);
    $this->created = (isset($arr['created'])) ? new DateTime($arr['created']) : new DateTime;
    $this->imgUrl = (isset($arr['image'])) ? $arr['image'] : null;
    $this->id = (isset($arr['id'])) ? $arr['id'] : null;
    $this->is_publication = (isset($arr['is_publication'])) ? intval($arr['is_publication']) : false;
  }

  /**
   * Return news id
   * 
   * @return int
   */
  public function getId(){
    return $this->id;
  }

  /**
   * Return news title
   *
   * @return string
   */
  public function getTitle () {
    return $this->title;
  }

  /**
   * Set news title
   *
   * @return void
   */
  public function setTitle ($title) {
    if (is_string($title)) {
      $this->title = $title;
    }
    else {
      throw new Exception("Title must be a string.");
    }
  }
  /**
   * Return text of news
   * @param int $mode - 1 - text will be minified
   *                    2 - text will be shorted for preview in news page
   *
   * @return string
   */
  public function getText ($mode = DEFAULT_MODE) {
    switch($mode) {
      case NEWS_MIN_MODE:
      case PUB_MIN_MODE:
        return substr($this->text, 0, MINIFIED_TEXT_LENGTH);
      case NEWS_PREVIEW_MODE:
      case PUB_PREVIEW_MODE:
        return substr($this->text, 0, PREVIEW_TEXT_LENGTH);
      default:
        return $this->text;
    }
  }

  /**
   * Set news title
   *
   * @return void
   */
  public function setText ($text) {
    if (is_string($text)) {
      $this->text = $text;
    }
    else {
      throw new Exception("Text must be a string.");
    }
  }

  /**
   * Return creation date
   * 
   * @return string
   */
  public function getCreated () {
    return date('Y-d-m H:i:s', $this->created->getTimestamp());
  }

  /**
   * Return imageUrl
   * 
   * @return string
   */
  public function getImageUrl () {
    return (string)($this->imgUrl);
  }

  /**
   * Returns is_publication
   * 
   * @return int;
   */
  public function isPublication () {
    return $this->is_publication;
  }

  /**
   * Render HTML code to display news
   *
   * @param int $mode
   *
   * @return string
   */
  public function render (int $mode = DEFAULT_MODE) {
    switch($mode){
      case NEWS_MIN_MODE:
      case NEWS_PREVIEW_MODE:
        $output = "";
        if ($this->imgUrl) {
          $output .= "
            <div class=\"media-left\">
              <a href=\"/news.php?id={$this->getId()}\">
                <img class=\"media-object\" 
                     src=\"{$this->imgUrl}\" alt=\"image\" 
                     width=\"60px\" height=\"60px\"/>
              </a>
            </div>";
        }
        $output .= "
          <div class=\"media-body\">
            <a href=\"/news.php?id={$this->getId()}\">
              <h4 class=\"media-heading\">
                {$this->getTitle()}
              </h4>
            </a>
            {$this->getText($mode)} <a href=\"/news.php?id={$this->getId()}\">Подробнее >></a>
          </div>";
        return $output;
      case PUB_MIN_MODE:
        $output = "";
        $output .= "
          <div class=\"col-sm-6 col-md-4\">
            <div class=\"thumbnail\">";
        $url = $this->getImageUrl();
        if ($url) {
            $output .= "<img src=\"{$url}\">";
        }
        $output .= "
          <div class=\"caption\">
            <h3>{$this->getTitle()}</h3>
            <p>{$this->getText($mode)}</p>
            <a href=\"/pubs.php?id={$this->getId()}\">Читать далее...</a>
          </div>
          </div>
          </div>
          ";
        return $output;
      case PUB_PREVIEW_MODE:
        $output = "";
        if ($this->imgUrl) {
          $output .= "
            <div class=\"media-left\">
              <a href=\"/pubs.php?id={$this->getId()}\">
                <img class=\"media-object\" 
                    src=\"{$this->imgUrl}\" alt=\"image\" 
                    width=\"100px\" height=\"100px\"/>
              </a>
            </div>";
        }
        $output .= "
          <div class=\"media-body\">
            <a href=\"/pubs.php?id={$this->getId()}\">
              <h4 class=\"media-heading\">
                {$this->getTitle()}
              </h4>
            </a>
            {$this->getText($mode)} <a href=\"/pubs.php?id={$this->getId()}\">Подробнее >></a>
          </div>";
        return $output;
      case ADMIN_MODE:
        // in admin panel
        $output = "<tr>
          <td>{$this->getId()}</td>
          <td>{$this->getTitle()}</td>
          <td>{$this->getCreated()}</td>
          ";
        if($this->isPublication() == 1) {
          $output .= "<td>Публикация</td>";
        }
        else {
          $output .= "<td>Новость</td>";
        }
        $output .= "</tr>";
        return $output;
      default:
        $output = "";
        $output .= "
          <div class=\"row\">
          <div class=\"col-xs-12 col-sm-8\">
              <h1 class=\"mt-4\">{$this->getTitle()}</h1>
              <!-- <p class=\"lead\">
                  by
                  <a href=\"#\">Start Bootstrap</a>
              </p> -->
          <hr>
          <p>От {$this->getCreated()}</p>
          <hr>";
        if($this->getImageUrl()) {
            $output .= "<img class=\"img-fluid rounded col-xs-12\" src=\"{$this->getImageUrl()}\">
            <hr>";
        }
        $output .= "<p>{$this->getText()}</p>
        </div>";
        return $output;
    }
    
  }

  /** 
   * Return news represented as array
   * 
   * @return array
   */
  public function toArray () {
    return [
      'title' =>  $this->title,
      'text' => $this->text,
      'created' => $this->created,
      'image' => $this->imgUrl
    ];
  }
}

class PubsService {
  /**
   * Save news in database
   *
   * @return int | boolean
   */
  public function save (Array $news = array()) {
    $news = new News($news);
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (mysqli_connect_errno()) {
      printf("<h1>Невозможно подключиться к базе данных.</h1><br>Код ошибки - %s", mysqli_connect_error());
      exit();
    }
    $query = $conn->prepare("INSERT INTO publications (title, text, image, created, is_publication) VALUES (?, ?, ?, ?, ?)");
    $title = $conn->real_escape_string($news->getTitle());
    $text = $conn->real_escape_string($news->getText());
    $imgUrl = $news->getImageUrl();
    $created = $news->getCreated();
    $is_publication = $news->isPublication();
    $query->bind_param(
      "ssssd", 
      $title,
      $text,
      $imgUrl,
      $created,
      $is_publication
    );
    $query->execute();    
    $inserted = $query->affected_rows;
    $query->close();
    $conn->close();
    return $inserted;
  }

  /**
   * Return filtered and limited news
   * 
   * @return array
   */
  public function getAll ($limit = 10, $offset = null, $is_publication = null) {
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (mysqli_connect_errno()) {
      printf("<h1>Невозможно подключиться к базе данных.</h1><br>Код ошибки - %s", mysqli_connect_error());
      exit();
    }
    $queryStr = "SELECT id, title, text, image, created, is_publication FROM `publications`";
    if($is_publication !== null) $queryStr .=" WHERE is_publication = " . strval(intval($is_publication));
    $queryStr .= " ORDER BY id DESC";
    if ($limit) $queryStr .= " LIMIT $limit";
    if ($offset) $queryStr .= " OFFSET $offset";
    $result = $conn->query($queryStr);
    if(!$result) {
      var_dump($conn->error);
    }
    $arr = [];
    while ($row = $result->fetch_assoc()) {
      $arr[] = new News($row);
    }
    $conn->close(); 
    return $arr;
  }

  /**
   * Return news object by id or false if it is impossible
   * 
   * @param id
   * @return News|boolean
   */
  public function getById ($id) {
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (mysqli_connect_errno()) {
      printf("<h1>Невозможно подключиться к базе данных.</h1><br>Код ошибки - %s", mysqli_connect_error());
      exit();
    }
    $queryStr = "SELECT id, title, text, image, created FROM `publications` WHERE id = {inval($id)}";
    $result = $conn->query($queryStr);
    if($result->num_rows == 1) return new News($result->fetch_assoc());
    else return false;
  }
}