<?php
    error_reporting(E_ALL);
    require('./Models/News.php');
    $pubsService = new PubsService();
    if(!(isset($_GET['id']) && intval($_GET['id']))) {
        $allPubs = $pubsService->getAll(10, null, true);
    }
    else {
        $pub = $pubsService->getById(intval($_GET['id']));
        if ($pub === false) header('Location: /pubs.php');
    }
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Портал корпоративной прозрачности</title>
    <link tyle="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="/">Главная</a></li>
            <li><a href="#">О нас</a></li>
            <li><a href="/ratings.php">Рейтинги</a></li>
            <li class="active"><a href="/pubs.php">Публикации</a></li>
            <li><a href="/news.php">Новости<span class="sr-only">(current)</span></a></li>
            <li><a href="#">База данных</a></li>
          </ul>
          <ul class="nav navbar-nav pull-right">
            <li><a href="/admin.php">Панель управления</a></li>
          </ul>

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <?php
                if(isset($allPubs)) :?>
                    <ul class="media-list">
                    <?php foreach($allPubs as $pubs) :?>
                        <div class="col-xs-12">
                            <?php echo $pubs->render(PUB_PREVIEW_MODE);?>
                        </div>
                    <?php endforeach; ?>
                    </ul>
            <?php elseif (isset($pub)) :?>
                <?php echo $pub->render(); ?>
            <?php else : ?>
            <p>Статей пока нет!</p>
            <?php endif; ?>
            </div>
        </div>
    </div>
  </body>
</div>