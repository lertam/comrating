-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 05 2017 г., 15:07
-- Версия сервера: 5.6.37
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `comrating`
--

-- --------------------------------------------------------

--
-- Структура таблицы `publications`
--

CREATE TABLE `publications` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL COMMENT 'path to image',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_publication` tinyint(1) DEFAULT '0' COMMENT 'publication or news'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `subranking`
--

CREATE TABLE `subranking` (
  `id` int(11) NOT NULL,
  `position` varchar(5) NOT NULL,
  `name` varchar(200) NOT NULL,
  `score` decimal(10,0) NOT NULL,
  `criteria` varchar(100) NOT NULL COMMENT 'Критерий субрэнкинга',
  `published` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `subrating`
--

CREATE TABLE `subrating` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'Наименование субрейтинга',
  `level` enum('premium','1','2','3','4','5') NOT NULL,
  `position` varchar(5) NOT NULL COMMENT 'Позиция компании',
  `name` varchar(200) NOT NULL,
  `infIndex` enum('A','B','C','D') NOT NULL,
  `score` float NOT NULL,
  `published` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subrating`
--

INSERT INTO `subrating` (`id`, `title`, `level`, `position`, `name`, `infIndex`, `score`, `published`) VALUES
(1, 'Субрейтинг частных компаний', 'premium', '1', 'Публичное акционерное общество «Горно-металлургическая компания «Норильский никель»', 'B', 63.88, '2017-10-05'),
(2, 'Субрейтинг частных компаний		', 'premium', '2', 'Акционерное общество «Сибирская Угольная Энергетическая Компания»', 'B', 55.75, '2017-10-05'),
(3, 'Субрейтинг частных компаний', 'premium', '3', 'Публичное Акционерное Общество «Нижнекамскнефтехим»', 'B', 51.5, '2017-10-05');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `subranking`
--
ALTER TABLE `subranking`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `subrating`
--
ALTER TABLE `subrating`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `publications`
--
ALTER TABLE `publications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `subranking`
--
ALTER TABLE `subranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `subrating`
--
ALTER TABLE `subrating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
